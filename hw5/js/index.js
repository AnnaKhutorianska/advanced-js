const root = document.querySelector('#root');

class API {
	constructor() {
		this._baseUrl = 'https://ajax.test-danit.com/api/json/';
	}

	async request(method, url) {
		const response = await fetch(this._baseUrl + url, {
			method,
		});
		return response;
	}

	get(url) {
		return this.request('GET', url);
	}

	delete(url) {
		return this.request('DELETE', url);
	}
}

class Card extends API {
	constructor(root) {
		super();
		this.root = root;
	}

	async getInfo() {
		try {
			return await Promise.all([super.get('users'), super.get('posts')]);
		} catch (e) {
			console.log(e);
		}
	}

	async deletePost(postId, e) {
		try {
			const response = await super.delete(`posts/${postId}`);
			console.log(postId, response.status);
			if(response.ok) {
				e.target.closest('li').remove();
			} else {
				throw response.status;
			}
 		} catch(e) {
			console.log(e);
		}
	}

	async render() {
		const ul = document.createElement('ul');
		const [users1, posts1] = await this.getInfo();
		const users = await users1.json();
		const posts = await posts1.json();
		
		users.forEach(({ id, name, email }) => {
			const filteredPosts = posts.filter(({ userId }) => userId == id);

			filteredPosts.forEach(({ id, title, body }) => {
				const li = document.createElement('li');
				li.className = 'post-card'
				const delButton = document.createElement('button');
				delButton.className = 'btn-delete'
				delButton.innerHTML = '&#x274C;';
				delButton.addEventListener('click', (e) => {
					const postId = e.target.closest('li').id;
					this.deletePost(postId, e);
				});
				li.setAttribute('id', id);
				li.innerHTML = `
					<span class='post-card__name'>${name}</span>
					<span class='post-card__email'>${email}</span>
					<h2 class='post-card__title'>${title}</h2>
					<p class='post-card__body'>${body}</p>`;
				li.append(delButton);
				ul.append(li);
			});
		});

		this.root.append(ul);
	}
}

const card = new Card(root);
card.render();
