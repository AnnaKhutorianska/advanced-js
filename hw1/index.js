class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        return this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        return this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        return this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        return this._lang = newLang;
    }

    get salary() {
        return this._salary * 3;
    }

}


const programmer1 = new Programmer("olha", 24, 200, ["en"]);
const programmer2 = new Programmer("anna", 22, 100, ["ru", "en"]);
const programmer3 = new Programmer("pasha", 21, 50, ["ru", "en", "ua"]);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
console.log(programmer1.salary);


