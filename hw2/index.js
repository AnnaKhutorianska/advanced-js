const root = document.querySelector('#root');
const books = [
	{ 
	  author: "Люсі Фолі",
	  name: "Список запрошених",
	  price: 70 
	}, 
	{
	 author: "Сюзанна Кларк",
	 name: "Джонатан Стрейндж і м-р Норрелл",
	}, 
	{ 
	  name: "Дизайн. Книга для недизайнерів.",
	  price: 70
	}, 
	{ 
	  author: "Алан Мур",
	  name: "Неономікон",
	  price: 70
	}, 
	{
	 author: "Террі Пратчетт",
	 name: "Рухомі картинки",
	 price: 40
	},
	{
	 author: "Анґус Гайленд",
	 name: "Коти в мистецтві",
	}
  ];

const showList = (arr, parent) => {
	const list = document.createElement('ul');
	const keyArr = ['author', 'name', 'price'];

	arr.forEach((elem) => {
		console.log(elem);
		try {
			const notIncludedKey = keyArr.filter((key) => !Object.keys(elem).includes(key));
			if (notIncludedKey.length >= 1) {
				throw `${notIncludedKey}`;
			} else {
				const elemList = document.createElement('li');
				elemList.textContent = `author: ${elem.author}, name: ${elem.name}, price: ${elem.price}`;
				list.append(elemList);
				parent.append(list);
			}
		} catch (error) {
			console.log(error);
		}
	});
};

showList(books, root);
