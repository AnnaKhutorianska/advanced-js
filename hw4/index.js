const filmsURL = 'https://ajax.test-danit.com/api/swapi/films';

const request = (url) => {
	return fetch(url)
		.then((response) => response.json())
		.catch((error) => console.log(error));
};

const getCharacters = ((id, urls) => {
	const charactersList =  Promise.all(urls.map((elem) => request(elem)))
	charactersList.then((data) => {
		console.log(data);
		data.forEach(({name}) => {
			document.getElementById(id).innerHTML += `<span> ${name} </span>`
		})
	})
});

const renderFilms = () => {
	request(filmsURL).then((data) => {
		const ul = document.createElement('ul');

		data.forEach(({ id, episodeId, openingCrawl, name, characters }) => {
			const li = document.createElement('li');
			li.innerHTML = `<p>${episodeId}</p> 
			<p>${name}</p>
			<p id="${id}"></p>
			<p>${openingCrawl}</p> `;
			ul.append(li);

			getCharacters(id, characters);
		});

		document.body.append(ul);
	});
};

renderFilms();
